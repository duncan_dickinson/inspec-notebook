# InSpec notebook

Key resources:

- [InSpec Tutorials](https://www.inspec.io/tutorials/)

To get started, fire up the InSpec shell via Docker and have a poke around:

````
docker run -it --rm chef/inspec shell
````

## Tutorial 1

The first tutorial is straight-forward and can just run straight in a Docker container.

````
docker run -it --rm -v $(pwd):/share chef/inspec exec /share/hello/hello_spec.rb
````